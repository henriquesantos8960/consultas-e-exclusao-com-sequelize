import AppError from "../../utils/AppError";
import model, {CustomerInput, CustomerOutput} from "../models/CustomersModel";

export const getAll = async (): Promise<CustomerOutput[]> => {
    return await model.findAll({
        include: {all: true},
    });
};

export const getById = async (id: number): Promise<CustomerOutput> => {
    const customer = await model.findOne({
        where: {
            customerNumber: id,
        },
        include: {all: true, nested: true},
    });

    if (!customer) {
        throw new AppError("NotFoundError", "Registro não encotrado", 404);
    };

    return customer;
};

export const create = async (payload: CustomerInput): Promise<CustomerOutput> => {
    return await model.create(payload);
};

export const updateById = async (id: number, payload: CustomerInput): Promise<CustomerOutput> => {
    const customer = await model.findByPk(id);

    if (!customer) {
        throw new Error("Registro não encontrado");
    };

    return await customer.update(payload);
};

export const deleteById = async (id: number): Promise<void> => {
    const customer = await model.findByPk(id);

    if (!customer) {
        throw new Error("Registro não encontrado");
    };

    await customer.destroy();
};